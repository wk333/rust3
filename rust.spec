%global bootstrap_rust 1.79.0
%global bootstrap_cargo 1.79.0
%global bootstrap_channel 1.79.0
%global bootstrap_date 2024-06-13
%global bootstrap_arches x86_64 aarch64 riscv64
%bcond_with llvm_static
%bcond_with bundled_llvm
%bcond_without bundled_libgit2
%bcond_with disabled_libssh2
%bcond_without lldb
%bcond_without analyzer

Name:                rust
Version:             1.80.0
Release:             1
Summary:             The Rust Programming Language
License:             Apache-2.0 OR MIT
URL:                 https://www.rust-lang.org
Source0:             https://static.rust-lang.org/dist/rustc-%{version}-src.tar.xz
Source1:             https://static.rust-lang.org/dist/rustc-%{version}-src.tar.xz.asc
Source2:             https://static.rust-lang.org/rust-key.gpg.ascii
# SOURCE3-5: use local mirror for speed up
Source3:             cargo-config
Source4:             cargo-config.sh
Source5:             cargo-config.csh

Patch0000:           rustc-1.80.0-disable-libssh2.patch
# By default, rust tries to use "rust-lld" as a linker for some targets.
Patch0001:           0001-Use-lld-provided-by-system.patch
# Set a substitute-path in rust-gdb for standard library sources.
Patch0002:           rustc-1.70.0-rust-gdb-substitute-path.patch
Patch0003:           0001-add-support-for-ppc64le.patch

%{lua: function rust_triple(arch)
  local abi = "gnu"
  if arch == "armv7hl" then
    arch = "armv7"
    abi = "gnueabihf"
  elseif arch == "ppc64" then
    arch = "powerpc64"
  elseif arch == "ppc64le" then
    arch = "powerpc64le"
  elseif arch == "riscv64" then
    arch = "riscv64gc"
  end
  return arch.."-unknown-linux-"..abi
end}
%{lua: function rust_musl_triple(arch)
  local abi = "musl"
  if arch == "riscv64" then
    arch = "riscv64gc"
  end
  return arch.."-unknown-linux-"..abi
end}
%{lua: function rust_musl_root(arch)
  if arch == "riscv64" then
    arch = "riscv64gc"
  end
  return "--musl-root-"..arch
end}
%global rust_triple %{lua: print(rust_triple(rpm.expand("%{_target_cpu}")))}
%global rust_musl_triple %{lua: print(rust_musl_triple(rpm.expand("%{_target_cpu}")))}
%global rust_musl_root %{lua: print(rust_musl_root(rpm.expand("%{_target_cpu}")))}
%if %defined bootstrap_arches
%{lua: do
  local bootstrap_arches = {}
  for arch in string.gmatch(rpm.expand("%{bootstrap_arches}"), "%S+") do
    table.insert(bootstrap_arches, arch)
  end
  local base = rpm.expand("https://static.rust-lang.org/dist/%{bootstrap_date}"
                          .."/rust-%{bootstrap_channel}")
  local target_arch = rpm.expand("%{_target_cpu}")
  for i, arch in ipairs(bootstrap_arches) do
    if arch == target_arch then
      print(string.format("Source%d: %s-%s.tar.xz\n",
                          i+5, base, rust_triple(arch)))
      rpm.define("bootstrap_source "..i+5)
    end
  end
end}
%endif
%ifarch %{bootstrap_arches}
%global bootstrap_root rust-%{bootstrap_channel}-%{rust_triple}
%global local_rust_root %{_builddir}/%{bootstrap_root}/usr
Provides:            bundled(%{name}-bootstrap) = %{bootstrap_rust}
%else
BuildRequires:       cargo >= %{bootstrap_cargo}
BuildRequires:       (%{name} >= %{bootstrap_rust} with %{name} <= %{version})
%global local_rust_root %{_prefix}
%endif
BuildRequires:       make gcc gcc-c++ ncurses-devel curl curl-devel musl-libc-static musl-gcc pkgconfig(libcurl) pkgconfig(liblzma)
BuildRequires:       pkgconfig(openssl) pkgconfig(zlib) pkgconfig(libssh2) >= 1.6.0 gnupg2 wget
%global python python3
BuildRequires:       %{python}
%if %with bundled_llvm
BuildRequires:       cmake3 >= 3.13.4
Provides:            bundled(llvm) = 17.0.4
%else
BuildRequires:       cmake >= 2.8.11
%if %defined llvm
%global llvm_root %{_libdir}/%{llvm}
%else
# default llvm is decent enough on riscv64
%global llvm llvm
%global llvm_root %{_prefix}
%endif
# rust currently requires llvm 14.0+
BuildRequires:       %{llvm} >= 15.0.0
BuildRequires:       %{llvm}-devel >= 15.0.0
%if %with llvm_static
BuildRequires:       %{llvm}-static libffi-devel
%endif
%endif
BuildRequires:       procps-ng
BuildRequires:       ninja-build
BuildRequires:       compiler-rt
BuildRequires:       clang
Provides:            rustc = %{version}-%{release}
Provides:            rustc%{?_isa} = %{version}-%{release}
Requires:            %{name}-std-static%{?_isa} = %{version}-%{release}
Requires:            /usr/bin/cc
%global _privatelibs lib(.*-[[:xdigit:]]{16}*|rustc.*)[.]so.*
%global __provides_exclude ^(%{_privatelibs})$
%global __requires_exclude ^(%{_privatelibs})$
%global __provides_exclude_from ^(%{_docdir}|%{rustlibdir}/src)/.*$
%global __requires_exclude_from ^(%{_docdir}|%{rustlibdir}/src)/.*$
%global _find_debuginfo_opts --keep-section .rustc
%global rustflags -Clink-arg=-Wl,-z,relro,-z,now
%if %{without bundled_llvm}
%if "%{llvm_root}" == "%{_prefix}" || 0%{?scl:1}
%global llvm_has_filecheck 1
%endif
%endif
%global musl_root %{_prefix}/musl

# The 'analysis' component is removed since Rust 1.69.0
# ref: https://github.com/rust-lang/rust/pull/101841
Obsoletes:             %{name}-analysis < 1.69.0~

%description
Rust is a systems programming language that runs blazingly fast, prevents
segfaults, and guarantees thread safety.
This package includes the Rust compiler and documentation generator.

%package std-static
Summary:             Standard library for Rust
%description std-static
This package includes the standard libraries for building applications
written in Rust.

%package debugger-common
Summary:             Common debugger pretty printers for Rust
BuildArch:           noarch
%description debugger-common
This package includes the common functionality for %{name}-gdb and %{name}-lldb.

%package gdb
Summary:             GDB pretty printers for Rust
BuildArch:           noarch
Requires:            gdb %{name}-debugger-common = %{version}-%{release}
%description gdb
This package includes the rust-gdb script, which allows easier debugging of Rust
programs.

%package lldb
Summary:             LLDB pretty printers for Rust
BuildArch:           noarch
Requires:            lldb %{python}-lldb
Requires:            %{name}-debugger-common = %{version}-%{release}

%description lldb
This package includes the rust-lldb script, which allows easier debugging of Rust
programs.

%package -n cargo
Summary:             Rust's package manager and build tool
%if %with bundled_libgit2
Provides:            bundled(libgit2) = 1.1.0
%endif
BuildRequires:       git
Requires:            rust
Obsoletes:           cargo-vendor <= 0.1.23
Provides:            cargo-vendor = %{version}-%{release}
%description -n cargo
Cargo is a tool that allows Rust projects to declare their various dependencies
and ensure that you'll always get a repeatable build.

%package -n rustfmt
Summary:             Tool to find and fix Rust formatting issues
Requires:            cargo
# /usr/bin/rustfmt is dynamically linked against internal rustc libs
Requires:            %{name}%{?_isa} = %{version}-%{release}
Obsoletes:           rustfmt-preview < 1.0.0
Provides:            rustfmt-preview = %{version}-%{release}
Conflicts:           rustfmt-preview < 1.0.0
%description -n rustfmt
A tool for formatting Rust code according to style guidelines.

%if %{with analyzer}
%package analyzer
Summary:Rust implementation of the Language Server Protocol
Requires:            %{name}-src
# RLS is no longer available as of Rust 1.65, but we're including the stub
# binary that implements LSP just enough to recommend rust-analyzer.
Obsoletes:           rls < 1.65.0~
Obsoletes:           rls-preview < 1.31.6

%description analyzer
rust-analyzer is an implementation of Language Server Protocol for the Rust
programming language. It provides features like completion and goto definition
for many code editors, including VS Code, Emacs and Vim.
%endif

%package -n clippy
Summary:             Lints to catch common mistakes and improve your Rust code
Requires:            cargo %{name}%{?_isa} = %{version}-%{release}
Obsoletes:           clippy-preview <= 0.0.212
Provides:            clippy-preview = %{version}-%{release}
Conflicts:           clippy-preview <= 0.0.212
%description -n clippy
A collection of lints to catch common mistakes and improve your Rust code.

%package src
Summary:             Sources for the Rust standard library
BuildArch:           noarch
%description src
This package includes source files for the Rust standard library.  It may be
useful as a reference for code completion tools in various editors.

%package help
Summary:     Help documents for rust

Provides:    %{name}-doc = %{version}-%{release} %{name}-cargo-doc = %{version}-%{release}
Obsoletes:   %{name}-doc < %{version}-%{release} %{name}-cargo-doc < %{version}-%{release}

%description help
Man pages and other related help documents for rust.

%prep
# download source0 and gpg check
wget -qO %{SOURCE0} https://user-repo.openeuler.openatom.cn/lfs-tar/rust/rustc-%{version}-src.tar.xz
gpg --import %{SOURCE2}
gpg --verify %{SOURCE1} %{SOURCE0}

%ifarch %{bootstrap_arches}
wget -qO %{_sourcedir}/%{bootstrap_root}.tar.xz https://user-repo.openeuler.openatom.cn/lfs-tar/rust/%{bootstrap_root}.tar.xz
%setup -q -n %{bootstrap_root} -T -b %{bootstrap_source}
./install.sh --components=cargo,rustc,rust-std-%{rust_triple} \
  --prefix=%{local_rust_root} --disable-ldconfig
test -f '%{local_rust_root}/bin/cargo'
test -f '%{local_rust_root}/bin/rustc'
%endif
%setup -q -n rustc-%{version}-src
%if %with disabled_libssh2
%patch -P 0000 -p1
%endif
%if "%{python}" != "python3"
sed -i.try-python -e '/^try python3 /i try "%{python}" "$@"' ./configure
%endif
%patch -P 0001 -p1
%patch -P 0002 -p1
%patch -P 0003 -p1
rm -rf vendor/curl-sys*/curl/
rm -rf vendor/jemalloc-sys/jemalloc/
rm -rf vendor/libffi-sys*/libffi/
rm -rf vendor/libssh2-sys*/libssh2/
rm -rf vendor/libz-sys*/src/zlib{,-ng}/
rm -rf vendor/lzma-sys*/xz-*/
rm -rf vendor/openssl-src*/openssl/
%if %without bundled_libgit2
rm -rf vendor/libgit2-sys*/libgit2/
%endif
%if %with disabled_libssh2
rm -rf vendor/libssh2-sys*/
%endif

# This only affects the transient rust-installer, but let it use our dynamic xz-libs
sed -i.lzma -e '/LZMA_API_STATIC/d' src/bootstrap/src/core/build_steps/tool.rs

%if %{without bundled_llvm} && %{with llvm_static}
sed -i.ffi -e '$a #[link(name = "ffi")] extern {}' \
  src/librustc_llvm/lib.rs
%endif
find vendor -name .cargo-checksum.json \
  -exec sed -i.uncheck -e 's/"files":{[^}]*}/"files":{ }/' '{}' '+'
find -name '*.rs' -type f -perm /111 -exec chmod -v -x '{}' '+'
%global rust_env RUSTFLAGS="%{rustflags}"
%if 0%{?cmake_path:1}
%global rust_env %{rust_env} PATH="%{cmake_path}:$PATH"
%endif
%if %without bundled_libgit2
%global rust_env %{rust_env} LIBGIT2_SYS_USE_PKG_CONFIG=1
%endif
%if %without disabled_libssh2
%global rust_env %{rust_env} LIBSSH2_SYS_USE_PKG_CONFIG=1
%endif

%build
export %{rust_env}
%global common_libdir %{_prefix}/lib
%global rustlibdir %{common_libdir}/rustlib
%ifarch %{arm} %{ix86} s390x
%define enable_debuginfo --debuginfo-level=0 --debuginfo-level-std=2
%else
%define enable_debuginfo --debuginfo-level=1
%endif
%ifnarch %{power64}
%define codegen_units_std --set rust.codegen-units-std=1
%endif
ncpus=$(/usr/bin/getconf _NPROCESSORS_ONLN)
max_cpus=$(( ($(free -g | awk '/^Mem:/{print $2}') + 1) / 2 ))
if [ "$max_cpus" -ge 1 -a "$max_cpus" -lt "$ncpus" ]; then
  ncpus="$max_cpus"
fi
# Find the compiler-rt library for the Rust profiler_builtins crate.
# But there are two versions in openEuler. Why?
# We don't have macros.clang so we need clang version here
# This is for avoiding rpm syntax error
%ifarch ppc64le
%global _arch powerpc64le
%endif
%global clang_maj_ver 17
%if %{?clang_maj_ver} >= 17
# This is the new one, used on openEuler 24.03 LTS or later
%define profiler %(echo %{_prefix}/%{_lib}/clang/%{clang_maj_ver}/lib/%{_arch}-%{_vendor}-linux-gnu/libclang_rt.profile.a)
%else
# This is used before openEuler 23.09
%global clang_full_ver %%(clang --version | awk '/clang version/{print $3}')
%define profiler %(echo %{_prefix}/%{_lib}/clang/%{clang_full_ver}/lib/libclang_rt.profile-%{_arch}.a)
%endif
test -r "%{profiler}"

%configure --disable-option-checking \
  --docdir=%{_pkgdocdir} \
  --libdir=%{common_libdir} \
  %{rust_musl_root}=%{musl_root} \
  --build=%{rust_triple} --host=%{rust_triple} --target=%{rust_triple},%{rust_musl_triple} \
  --set target.%{rust_triple}.profiler="%{profiler}" \
  --python=%{python} \
  --local-rust-root=%{local_rust_root} \
  %{!?with_bundled_llvm: --llvm-root=%{llvm_root} \
    %{!?llvm_has_filecheck: --disable-codegen-tests} \
    %{!?with_llvm_static: --enable-llvm-link-shared } } \
  --disable-rpath \
  %{enable_debuginfo} \
  --enable-extended \
  --tools=cargo,clippy,%{?with_analyzer:rls,rust-analyzer,}rustfmt,src \
  --enable-vendor \
  --enable-verbose-tests \
  %{?codegen_units_std} \
  --release-channel=stable
%{python} ./x.py build -j "$ncpus" --stage 2
%{python} ./x.py doc --stage 2

%install
export %{rust_env}
DESTDIR=%{buildroot} %{python} ./x.py install

# Some of the components duplicate-install binaries, leaving backups we don't want
rm -f %{buildroot}%{_bindir}/*.old

%if "%{_libdir}" != "%{common_libdir}"
mkdir -p %{buildroot}%{_libdir}
find %{buildroot}%{common_libdir} -maxdepth 1 -type f -name '*.so' \
  -exec mv -v -t %{buildroot}%{_libdir} '{}' '+'
%endif
find %{buildroot}%{_libdir} -maxdepth 1 -type f -name '*.so' \
  -exec chmod -v +x '{}' '+'
(cd "%{buildroot}%{rustlibdir}/%{rust_triple}/lib" &&
 find ../../../../%{_lib} -maxdepth 1 -name '*.so' |
 while read lib; do
   if [ -f "${lib##*/}" ]; then
     # make sure they're actually identical!
     cmp "$lib" "${lib##*/}"
     ln -v -f -s -t . "$lib"
   fi
 done)
find %{buildroot}%{rustlibdir} -maxdepth 1 -type f -exec rm -v '{}' '+'
find %{buildroot}%{rustlibdir} -type f -name '*.orig' -exec rm -v '{}' '+'
find %{buildroot}%{rustlibdir}/src -type f -name '*.py' -exec rm -v '{}' '+'
rm -f %{buildroot}%{_pkgdocdir}/README.md
rm -f %{buildroot}%{_pkgdocdir}/COPYRIGHT
rm -f %{buildroot}%{_pkgdocdir}/LICENSE
rm -f %{buildroot}%{_pkgdocdir}/LICENSE-APACHE
rm -f %{buildroot}%{_pkgdocdir}/LICENSE-MIT
rm -f %{buildroot}%{_pkgdocdir}/LICENSE-THIRD-PARTY
rm -f %{buildroot}%{_pkgdocdir}/*.old
find %{buildroot}%{_pkgdocdir}/html -empty -delete
find %{buildroot}%{_pkgdocdir}/html -type f -exec chmod -x '{}' '+'
mkdir -p %{buildroot}%{_datadir}/cargo/registry
mkdir -p %{buildroot}%{_docdir}/cargo
ln -sT ../rust/html/cargo/ %{buildroot}%{_docdir}/cargo/html

# install default config for cargo mirror
install -m 0644 -D -p %{SOURCE3} %{buildroot}%{_sysconfdir}/skel/.cargo/config.toml
install -m 0644 -D -p %{SOURCE4} %{buildroot}%{_sysconfdir}/profile.d/cargo-config.sh
install -m 0644 -D -p %{SOURCE5} %{buildroot}%{_sysconfdir}/profile.d/cargo-config.csh

%if %{with analyzer}
# The rls stub doesn't have an install target, but we can just copy it.
%{__install} -t %{buildroot}%{_bindir} build/%{rust_triple}/stage2-tools-bin/rls
%endif

%if %without lldb
rm -f %{buildroot}%{_bindir}/rust-lldb
rm -f %{buildroot}%{rustlibdir}/etc/lldb_*
%endif
rm -f %{buildroot}%{rustlibdir}/%{rust_triple}/bin/rust-ll*

%check
export %{rust_env}
%{python} ./x.py test --no-fail-fast --stage 2 || :
%ifarch aarch64
# https://github.com/rust-lang/rust/issues/123733
%define cargo_test_skip --test-args "--skip panic_abort_doc_tests"
%endif
%{python} ./x.py test --no-fail-fast --stage 2 cargo %{?cargo_test_skip} || :
%{python} ./x.py test --no-fail-fast --stage 2 clippy || :
%if %{with analyzer}
%{python} ./x.py test --no-fail-fast --stage 2 rust-analyzer || :
%endif
%{python} ./x.py test --no-fail-fast --stage 2 rustfmt || :
%ldconfig_scriptlets

%files
%license COPYRIGHT LICENSE-APACHE LICENSE-MIT
%license %{_pkgdocdir}/html/*.txt
%doc README.md
%{_bindir}/rustc
%{_bindir}/rustdoc
%{_libdir}/*.so
%dir %{rustlibdir}
%dir %{rustlibdir}/%{rust_triple}
%dir %{rustlibdir}/%{rust_triple}/lib
%if %{with analyzer}
%{_libexecdir}/rust-analyzer-proc-macro-srv
%endif
%{rustlibdir}/%{rust_triple}/lib/*.so
%dir %{rustlibdir}/%{rust_musl_triple}
%dir %{rustlibdir}/%{rust_musl_triple}/lib

%files std-static
%dir %{rustlibdir}
%dir %{rustlibdir}/%{rust_triple}
%dir %{rustlibdir}/%{rust_triple}/lib
%{rustlibdir}/%{rust_triple}/lib/*.rlib
%dir %{rustlibdir}/%{rust_musl_triple}
%dir %{rustlibdir}/%{rust_musl_triple}/lib
%{rustlibdir}/%{rust_musl_triple}/lib/*.rlib
%{rustlibdir}/%{rust_musl_triple}/lib/self-contained/*.o
%{rustlibdir}/%{rust_musl_triple}/lib/self-contained/libunwind.a
%{rustlibdir}/%{rust_musl_triple}/lib/self-contained/libc.a

%files debugger-common
%dir %{rustlibdir}
%dir %{rustlibdir}/etc
%{rustlibdir}/etc/rust_*.py*

%files gdb
%{_bindir}/rust-gdb
%{rustlibdir}/etc/gdb_*
%exclude %{_bindir}/rust-gdbgui
%if %with lldb

%files lldb
%{_bindir}/rust-lldb
%{rustlibdir}/etc/lldb_*
%endif

%files -n cargo
%license src/tools/cargo/LICENSE-APACHE src/tools/cargo/LICENSE-MIT src/tools/cargo/LICENSE-THIRD-PARTY
%doc src/tools/cargo/README.md
%config(noreplace) %{_sysconfdir}/skel/.cargo/config.toml
%{_sysconfdir}/profile.d/cargo-config.*
%{_bindir}/cargo
%{_sysconfdir}/bash_completion.d/cargo
%{_datadir}/zsh/site-functions/_cargo
%dir %{_datadir}/cargo
%dir %{_datadir}/cargo/registry

%files -n rustfmt
%{_bindir}/rustfmt
%{_bindir}/cargo-fmt
%doc src/tools/rustfmt/{README,CHANGELOG,Configurations}.md
%license src/tools/rustfmt/LICENSE-{APACHE,MIT}

%if %{with analyzer}
%files analyzer
%{_bindir}/rls
%{_bindir}/rust-analyzer
%doc src/tools/rust-analyzer/README.md
%license src/tools/rust-analyzer/LICENSE-{APACHE,MIT}
%endif

%files -n clippy
%{_bindir}/cargo-clippy
%{_bindir}/clippy-driver
%doc src/tools/clippy/{README.md,CHANGELOG.md}
%license src/tools/clippy/LICENSE-{APACHE,MIT}

%files src
%dir %{rustlibdir}
%{rustlibdir}/src

%files help
%dir %{_pkgdocdir}
%docdir %{_pkgdocdir}
%{_pkgdocdir}/html
%dir %{_docdir}/cargo
%docdir %{_docdir}/cargo
%{_docdir}/cargo/html
%{_mandir}/man1/rustc.1*
%{_mandir}/man1/rustdoc.1*
%{_mandir}/man1/cargo*.1*

%changelog
* Wed Aug 07 2024 wangkai <13474090681@163.com> - 1.80.0-1
- Update to 1.80.0

* Wed Jul 24 2024 wangkai <13474090681@163.com> - 1.79.0-3
- Switch to bootstrap compilation

* Thu Jul 18 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.79.0-2
- Drop obsolete patch for riscv64 (rust-lang/rust#123612 landed in 1.79.0)

* Wed Jun 19 2024 wangkai <13474090681@163.com> - 1.79.0-1
- Update to 1.79.0

* Thu Jun 13 2024 jianchunfu <chunfu.jian@shingroup.cn> - 1.78.0-2
- spec: Add support for ppc64le

* Tue May 07 2024 wangkai <13474090681@163.com> - 1.78.0-1
- Update to 1.78.0

* Mon Apr 22 2024 panchenbo <panchenbo@kylinsec.com.cn> - 1.77.0-3
- Modify openEuler to vendor

* Thu Apr 11 2024 misaka00251 <liuxin@iscas.ac.cn> - 1.77.0-2
- Enable profiler builtin

* Wed Apr 03 2024 wangkai <13474090681@163.com> - 1.77.0-1
- Update to 1.77.0

* Tue Feb 20 2024 wangkai <13474090681@163.com> - 1.76.0-1
- Update to 1.76.0

* Sat Feb 17 2024 wangkai <13474090681@163.com> - 1.75.0-2
- Fix CVE-2024-24575,CVE-2024-24577

* Wed Jan 10 2024 wangkai <13474090681@163.com> - 1.75.0-1
- Update to 1.75.0

* Fri Nov 24 2023 wangkai <13474090681@163.com> - 1.74.0-1
- Update to 1.74.0

* Mon Nov 13 2023 wangkai <13474090681@163.com> - 1.73.0-2
- Remove git lfs tar and change to user-repo
- Add gpg verification to Source0

* Mon Oct 09 2023 wangkai <13474090681@163.com> - 1.73.0-1
- Update to 1.73.0

* Mon Aug 28 2023 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.72.0-1
- Update to 1.72.0

* Mon Aug 7 2023 Funda Wang <fundawang@yeah.net> - 1.71.1-1
- New version 1.71.1

* Sun Jul 30 2023 Funda Wang <fundawang@yeah.net> - 1.71.0-3
- Fix release channel name

* Sun Jul 30 2023 Funda Wang <fundawang@yeah.net> - 1.71.0-2
- Use local mirror for speed up

* Fri Jul 28 2023 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.71.0-1
- Update to 1.71.0

* Tue Jul 18 2023 xu_ping <707078654@qq.com> - 1.70.0-2
- Use llvm package instead of llvm15

* Mon Jun 05 2023 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.70.0-1
- Update to 1.70.0
- Fix rotten patch

* Mon Apr 24 2023 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.69.0-2
- Add riscv64 specific changes

* Mon Apr 24 2023 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.69.0-1
- Update to 1.69.0
- Obsolete the removed rust-analysis subpackage
- Switch to xz tarball to save space

* Wed Mar 22 2023 wangkai <wangkai385@h-partners.com> - 1.68.0-1
- Update to 1.68.0

* Tue Feb 28 2023 wangkai <wangkai385@h-partners.com> - 1.67.1-1
- Update to 1.67.1

* Tue Apr 19 2022 Liu Zixian <liuzixian4@huawei.com> - 1.60.0-1
- Update to 1.60.0

* Mon Feb 28 2022 Liu Zixian <liuzixian4@huawei.com> - 1.59.0-1
- Update to 1.59.0

* Sun Feb 27 2022 Liu Zixian <liuzixian4@huawei.com> - 1.58.1-1
- Update to 1.58.1

* Wed Feb 09 2022 Li Zheng <lizheng135@huawei.com> - 1.57.0-2
- Fix build error

* Sat Jan 22 2022 Liu Zixian <liuzixian4@huawei.com> - 1.57.0-1
- Update to 1.57.0

* Sat Dec 18 2021 sdlzx <hdu_sdlzx@163.com> - 1.56.0-1
- Update to 1.56.0

* Wed Dec 15 2021 sdlzx <hdu_sdlzx@163.com> - 1.55.0-1
- Update to 1.55.0

* Thu Oct 14 2021 sdlzx <hdu_sdlzx@163.com> - 1.54.0-1
- Update to 1.54.0

* Fri Oct 08 2021 donglongtao <donglongtao@huawei.com> - 1.53.0-2
- Update debuginfo-level config

* Sat Oct 02 2021 sdlzx <hdu_sdlzx@163.com> - 1.53.0-1
- Update to 1.53.0

* Tue Sep 28 2021 sdlzx <hdu_sdlzx@163.com> - 1.52.1-1
- Update to 1.52.1

* Fri Sep 17 2021 donglongtao <donglongtao@huawei.com> - 1.51.0-10
- Fix rustdoc install very slow

* Tue Aug 24 2021 caodongxia <caodongxia@huawei.com> - 1.51.0-9
- Fix rustdoc error info 

* Wed Aug 18 2021 yaoxin <yaoxin30@huawei.com> - 1.51.0-8
- Fix CVE-2021-29922

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.51.0-7
- DESC: delete BuildRequires gdb

* Thu Jul 08 2021 Jiajie Li <lijiajie11@huawei.com> - 1.51.0-6
- Add build require of ninja and llvm

* Thu Jul 01 2021 Jiajie Li <lijiajie11@huawei.com> - 1.51.0-5
- Add support for musl target

* Thu Jun 24 2021 sunguoshuai <sunguoshuai@huawei.com> - 1.51.0-4
- fix a println wrong format

* Thu Jun 24 2021 sunguoshuai <sunguoshuai@huawei.com> - 1.51.0-3
- cargo help clippy should have description to user

* Wed Jun 23 2021 sunguoshuai <sunguoshuai@huawei.com> - 1.51.0-2
- clippy-driver usage should user friendly

* Fri May 07 2021 wangyue <wangyue92@huawei.com> - 1.51.0-1
- Update to 1.51.0

* Mon Nov 30 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.45.2-2
- fix upgrade error

* Mon Sep 21 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.45.2-1
- Update to 1.45.2

* Fri Apr 17 2020 zhujunhao <zhujunhao8@huawei.com> - 1.29.1-4
- add llvm in rust

* Thu Dec 5 2019 wutao <wutao61@huawei.com> - 1.29.1-3
- Package init
