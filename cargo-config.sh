# Copy cargo config from skel if it is not exist
if [ ! -f "$HOME/.cargo/config.toml" ] ; then
        mkdir -p $HOME/.cargo
        cp -f /etc/skel/.cargo/config.toml $HOME/.cargo
fi
